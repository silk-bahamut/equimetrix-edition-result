# Projet Equimetrix Edition Result

Le but est de générer des fichiers pdf correspondant à nos besoins en nous appuyant sur les visuels du logiciel Equimetrix©

Le fichier entrée est un fichier excel.
La sortie est en pdf.

## Installation

 * Récupérer les sources du projet
    * soit en zip avec ce [lien](https://bitbucket.org/silk-bahamut/equimetrix-edition-result/get/69fb58761892.zip)
    * soit au travers de git avec la commande `git clone git@bitbucket.org:silk-bahamut/equimetrix-edition-result.git`
 * Installer [maven](https://maven.apache.org/install.html)
 * Installer [Java 8 (jdk)] (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
 * Lancer la commande `mvn clean package` dans le répertoire racine du projet
 * Dans le répertoire target un fichier zip sera généré. Il est aussi disponible [**ici**](https://bitbucket.org/silk-bahamut/equimetrix-edition-result/downloads/equimetrix-edition-result.zip)


## Utilisation
###Java FX
 * Si nécessaire installer [java 8 (jre)] (https://www.java.com/fr/download/)
    * Pour tester la version locale : `java -version` dans une invite de commande (touche Windows + R, taper cmd)
    > C:\\>java -version  
      java version "1.8.0_40"  
      Java(TM) SE Runtime Environment (build 1.8.0_40-b26)  
      Java HotSpot(TM) 64-Bit Server VM (build 25.40-b25, mixed mode)  
 * Télécharger l'archive déjà construit et partagée sur [bitbucket](https://bitbucket.org/silk-bahamut/equimetrix-edition-result/downloads/equimetrix-edition-result.zip)
 * Décompresser la dans un répertoire
 * Dans le dossier `bin` lancer l'executable `start-javafx.bat`
    1. Si **wkhtmltopdf** n'est pas installé alors l'IHM le signalera et ressemblera à ça :  
        ![wkhtmltopdf non installé](doc/javafx/pdfConverterKO.PNG)  
        1. `Site de téléchargement` : lancer un browser sur la page de téléchargement de [**wkhtmltopdf**](http://wkhtmltopdf.org/downloads.html)
        2. `Vérifier l'installation` : vérifie l'existance de l'executable dans les répertoires d'installation par défaut, si il est présent, l'IHM passe à la suite
    2. Une fois **wkhtmltopdf** correctement installé, l'IHM se présentera ainsi :  
        ![Sélection des fichiers](doc/javafx/pdfConverterOK.PNG)  
        1. `Choisir excel` : ouvre une popup de sélection de fichier xls, xlsx ou xlsm
        2. `Répertoire de travail` : ouvre une popup de sélection de répertoire pour donner le répertoire de travail de l'application
    3. Une fois le fichier excel et le répertoire saisi, le bouton de génération apparaît :  
        ![Génération OK](doc/javafx/fileSelected.PNG)  
    4. En cliquant sur le bouton, la barre de progression s'affiche, en indéterminée le temps de traiter le fichier excel puis en pourcentage une fois la génération effectivement en cours.  
        ![Génération en cours](doc/javafx/duringGeneration.PNG)

## Outils de développement
 * Java : [Java 8 (jdk)] (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
 * Gestion des sources : git, [git bash] (https://git-for-windows.github.io/)
 * IDE : [IntelliJ] (https://www.jetbrains.com/idea/download/)
 * Gestion des dépendances : [Maven] (https://maven.apache.org/install.html)
 * Edition des interfaces Java FX : [SceneBuilder] (http://gluonhq.com/open-source/scene-builder/)
 * Conversion HTML vers PDF : [wkhtmltopdf](http://wkhtmltopdf.org/downloads.html)
 * Affichage des radars : [Chart.js](http://www.chartjs.org/)
 
## Librairies java utilisées
 * [POI](https://poi.apache.org/) : Lecture de fichiers excel
 * [Jackson](http://wiki.fasterxml.com/JacksonHome) : conversion de POJO vers du json
 * [Commons-IO](https://commons.apache.org/proper/commons-io/) : gestion des fichiers en java 
 * [Java FX](http://docs.oracle.com/javase/8/javase-clienttechnologies.htm) : clients lourds java intégrés à Java
 
## Fonctionnement de la conversion
La lecture du fichier excel par POI, permet d'initialiser une liste de POJO représentant les chevaux et leurs stats.  
Ces données sont ensuite converties en json par Jackson data-bind puis injecter dans un fichier javascript.  
Ceci permet d'avoir la donnée manquante pour l'affichage de la page HTML  
Celle-ci est ensuite convertie en pdf avec wkhtmltopdf
 
