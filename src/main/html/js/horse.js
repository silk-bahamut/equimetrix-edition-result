var horse={
  "name" : "AQUI DU BARRALY",
  "sire" : "10322706",
  "age" : 5,
  "race" : "SF",
  "father" : "FOLAMOUR DU BOIS",
  "sex" : "H",
  "raw" : {
    "W" : {
      "type" : "W",
      "speed" : 6.28,
      "strideFrequency" : 0.91,
      "verticalPower" : 2.56,
      "verticalDisplacement" : 8.26,
      "symmetry" : 421.98,
      "regularity" : 304.19,
      "strideLength" : 1.96,
      "lateralPower" : 1.96,
      "horizontalPower" : 1.93
    },
    "TT" : {
      "type" : "TT",
      "speed" : 16.38,
      "strideFrequency" : 1.22,
      "verticalPower" : 10.95,
      "verticalDisplacement" : 10.77,
      "symmetry" : 294.21,
      "regularity" : 373.6,
      "strideLength" : 2.82,
      "lateralPower" : 2.4,
      "horizontalPower" : 7.61
    },
    "TM" : {
      "type" : "TM",
      "speed" : 19.49,
      "strideFrequency" : 1.38,
      "verticalPower" : 17.49,
      "verticalDisplacement" : 9.31,
      "symmetry" : 295.57,
      "regularity" : 350.82,
      "strideLength" : 3.15,
      "lateralPower" : 3.64,
      "horizontalPower" : 12.62
    },
    "GT" : {
      "type" : "GT",
      "speed" : 20.38,
      "strideFrequency" : 1.72,
      "verticalPower" : 14.84,
      "verticalDisplacement" : 18.88,
      "symmetry" : null,
      "regularity" : 133.57,
      "strideLength" : 2.93,
      "lateralPower" : 4.31,
      "horizontalPower" : 9.52
    },
    "GM" : {
      "type" : "GM",
      "speed" : 21.66,
      "strideFrequency" : 1.74,
      "verticalPower" : 19.44,
      "verticalDisplacement" : 21.08,
      "symmetry" : null,
      "regularity" : 115.72,
      "strideLength" : 3.58,
      "lateralPower" : 6.38,
      "horizontalPower" : 19.18
    }
  },
  "computed" : {
    "W" : {
      "type" : "W",
      "speed" : 38.0,
      "strideFrequency" : 62.0,
      "verticalPower" : 100.0,
      "verticalDisplacement" : 99.0,
      "symmetry" : 99.0,
      "regularity" : 99.0,
      "strideLength" : 38.0,
      "lateralPower" : 90.0,
      "horizontalPower" : 18.0
    },
    "TT" : {
      "type" : "TT",
      "speed" : 98.0,
      "strideFrequency" : 3.0,
      "verticalPower" : 18.0,
      "verticalDisplacement" : 57.0,
      "symmetry" : 81.0,
      "regularity" : 66.0,
      "strideLength" : 97.0,
      "lateralPower" : 41.0,
      "horizontalPower" : 36.0
    },
    "TM" : {
      "type" : "TM",
      "speed" : 98.0,
      "strideFrequency" : 12.0,
      "verticalPower" : 28.0,
      "verticalDisplacement" : 34.0,
      "symmetry" : 87.0,
      "regularity" : 70.0,
      "strideLength" : 87.0,
      "lateralPower" : 42.0,
      "horizontalPower" : 71.0
    },
    "GT" : {
      "type" : "GT",
      "speed" : 89.0,
      "strideFrequency" : 76.0,
      "verticalPower" : 51.0,
      "verticalDisplacement" : 26.0,
      "symmetry" : null,
      "regularity" : 49.0,
      "strideLength" : 20.0,
      "lateralPower" : 23.0,
      "horizontalPower" : 30.0
    },
    "GM" : {
      "type" : "GM",
      "speed" : 35.0,
      "strideFrequency" : 61.0,
      "verticalPower" : 58.0,
      "verticalDisplacement" : 44.0,
      "symmetry" : null,
      "regularity" : 40.0,
      "strideLength" : 38.0,
      "lateralPower" : 32.0,
      "horizontalPower" : 52.0
    }
  }
};
