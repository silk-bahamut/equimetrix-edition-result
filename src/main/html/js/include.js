var options = {
    // Boolean - If we want to override with a hard coded scale
    scaleOverride: true,

    // ** Required if scaleOverride is true **
    // Number - The number of steps in a hard coded scale
    scaleSteps: 4,
    // Number - The value jump in the hard coded scale
    scaleStepWidth: 25,
    // Number - The scale starting value
    scaleStartValue: 0,
    // String - Colour of the scale line
    scaleLineColor: "rgba(0,0,0,.3)",

    // Number - Pixel width of the scale line
    scaleLineWidth: 1,

    // Boolean - Whether to animate the chart
    animation: false,

    //Boolean - Whether to show lines for each scale point
    scaleShowLine: true,

    //Boolean - Whether we show the angle lines out of the radar
    angleShowLineOut: true,

    //Boolean - Whether to show labels on the scale
    scaleShowLabels: true,

    // Boolean - Whether the scale should begin at zero
    scaleBeginAtZero: false,

    //String - Colour of the angle line
    angleLineColor: "rgba(0,0,0,.3)",

    //Number - Pixel width of the angle line
    angleLineWidth: 1,

    //String - Point label font declaration
    pointLabelFontFamily: "'Arial'",

    //String - Point label font weight
    pointLabelFontStyle: "normal",

    //Number - Point label font size in pixels
    pointLabelFontSize: 10,

    //String - Point label font colour
    pointLabelFontColor: "rgba(0,0,0,1)",

    //Boolean - Whether to show a dot for each point
    pointDot: false,

    //Number - Radius of each point dot in pixels
    pointDotRadius: 3,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,

    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill: true

};
var headers = {
    speed: "Vitesse",
    strideFrequency: "Cadence",
    strideLength: "Longueur Foulée",
    verticalPower: "Puissance Verticale",
    verticalDisplacement: "Déplacement Vertical",
    regularity: "Régularité",
    symmetry: "Symétrie",
    lateralPower: "Puissance Latérale",
    horizontalPower: "Puissance Horizontale"
};
var units = {
    speed: {value: 'km/h', before: false},
    strideFrequency: {value: 'f/s', before: false},
    strideLength: {value: 'm', before: false},
    verticalPower: {value: 'W/kg', before: false},
    verticalDisplacement: {value: 'cm', before: false},
    regularity: {value: 'REG', before: true},
    symmetry: {value: 'SYM', before: true},
    lateralPower: {value: 'W/kg', before: false},
    horizontalPower: {value: 'W/kg', before: false},
};
var average = {
    speed: 50,
    strideFrequency: 50,
    strideLength: 50,
    verticalPower: 50,
    verticalDisplacement: 50,
    regularity: 50,
    symmetry: 50,
    lateralPower: 50,
    horizontalPower: 50
};

var legend = {
    "raw" : {
        "TM" : {
            "type" : "TM",
            "speed" : 16.53,
            "strideFrequency" : 1.38,
            "verticalPower" : 21.33,
            "verticalDisplacement" : 10.83,
            "symmetry" : 226.57,
            "regularity" : 326.46,
            "strideLength" : 3.17,
            "lateralPower" : 4.07,
            "horizontalPower" : 20.11
        }
    },
    "computed" : {
        "TM" : {
            "type" : "TM",
            "speed" : 75.0,
            "strideFrequency" : 13.0,
            "verticalPower" : 80.0,
            "verticalDisplacement" : 71.0,
            "symmetry" : 53.0,
            "regularity" : 49.0,
            "strideLength" : 90.0,
            "lateralPower" : 58.0,
            "horizontalPower" : 97.0
        }
    }
};
function canvas_arrow(context, fromx, fromy, tox, toy){
    var headlen = 10;   // length of head in pixels
    var angle = Math.atan2(toy-fromy,tox-fromx);
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
}
function generateRadarArray(obj, reference) {
    var array = [];
    [
        "verticalDisplacement",
        "verticalPower",
        "lateralPower",
        "horizontalPower",
        "regularity",
        "strideFrequency",
        "strideLength",
        "symmetry",
        "totalPower",
        "speed"
    ].forEach(function (element) {
        if (reference[element] > 0) {
            array.push(obj[element]);
        }
    });
    return array;
}

function generateRadar(obj) {
    var rgbaHorse = "28,65,186";
    var rgbaAvg = "22,186,33";

    var dataSetArray = [];
        dataSetArray.push({
            fillColor: "rgba(" + rgbaHorse + ",0.2)",
            strokeColor: "rgba(" + rgbaHorse + ",1)",
            strokeWidth: 5,
            data: generateRadarArray(obj, obj)
        });
    dataSetArray.push({
        fillColor: "rgba(" + rgbaAvg + ",0)",
        strokeColor: "rgba(" + rgbaAvg + ",1)",
        data: generateRadarArray(average, obj)
    });
    return {
        labels: generateRadarArray(headers, obj),
        datasets: dataSetArray
    };
}

function copyAllure(name, data, allureType) {
    var raw = data.raw[allureType];
    copyToDom(name, "speed", raw);
    copyToDom(name, "strideFrequency", raw);
    copyToDom(name, "strideLength", raw);
    copyToDom(name, "verticalPower", raw);
    copyToDom(name, "verticalDisplacement", raw);
    copyToDom(name, "regularity", raw);
    copyToDom(name, "symmetry", raw);
    copyToDom(name, "horizontalPower", raw);

    var computed = data.computed[allureType];
    var canvas = document.getElementById(name + "-radar");
    canvas.width = 400;
    canvas.height = 300;
    new Chart(canvas.getContext("2d")).Radar(generateRadar(computed), options);
}

function copyToDom(name, subId, raw) {
    if (raw[subId] != null) {
        var unit = units[subId];
        var digit = 'regularity' == subId || 'symmetry' == subId ? 0 : 1;
        var html;
        if (unit.before) {
            html = unit.value + ' ' + raw[subId].toFixed(digit);
        } else {
            html = raw[subId].toFixed(digit) + ' ' + unit.value;
        }
        document.getElementById(name + "-" + subId).innerHTML = html;
    }
}

function install(horse) {
    var arr = document.getElementsByClassName("horse-name");
    for (var i = 0; i < arr.length; i++) {
        arr[i].innerHTML = horse.name;
    }
    arr = document.getElementsByClassName("horse-father");
    for (i = 0; i < arr.length; i++) {
        arr[i].innerHTML = '(par ' + horse.father + ')';
    }
    arr = document.getElementsByClassName("horse-stats");
    for (i = 0; i < arr.length; i++) {
        arr[i].innerHTML = horse.sex + ' - ' + horse.race + ' - ' + horse.age + ' ans';
    }
    copyAllure("legend", legend, 'TM');
    copyAllure("walk", horse, 'W');
    copyAllure("working_trot", horse, 'TT');
    copyAllure("avg_trot", horse, 'TM');
    copyAllure("working_canter", horse, 'GT');
    copyAllure("avg_canter", horse, 'GM');

    // draw lines for legend (should be at the end
    var ctx = document.getElementById("legend-canvas").getContext("2d");
    ctx.beginPath();
    ctx.lineWidth = 3;
    // inferior
    canvas_arrow(ctx, 450, 280, 565, 175);
    // equals
    canvas_arrow(ctx, 610, 275, 600, 210);
    // superior
    canvas_arrow(ctx, 640, 10, 675, 205);
    // value
    canvas_arrow(ctx, 180, 360, 180, 340);
    // median
    canvas_arrow(ctx, 500, 15, 525, 110);
    ctx.stroke();

    ctx = document.getElementById("legend-canvas").getContext("2d");
    ctx.beginPath();
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 5;

    ctx.stroke();
}