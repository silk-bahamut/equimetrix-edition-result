package org.silk.horse.entity;

import java.util.EnumMap;

public class Horse {
    private String name;
    private String sire;
    private int age;
    private String race;
    private String father;
    private String sex;
    private EnumMap<AllureType, Allure> raw;
    private EnumMap<AllureType, Allure> computed;

    public Horse(String sire) {
        this.sire = sire;
        raw = new EnumMap<>(AllureType.class);
        computed = new EnumMap<>(AllureType.class);
    }


    public void addRaw(Allure allure) {
        raw.put(allure.getType(), allure);
    }

    public void addComputed(Allure allure) {
        computed.put(allure.getType(), allure);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSire() {
        return sire;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public EnumMap<AllureType, Allure> getRaw() {
        return raw;
    }

    public EnumMap<AllureType, Allure> getComputed() {
        return computed;
    }
}
