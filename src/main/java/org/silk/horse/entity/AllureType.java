package org.silk.horse.entity;

public enum AllureType {
    W, TT, TM, GT, GM
}
