package org.silk.horse.entity;

public class Allure {
    private AllureType type;
    private Double speed;
    private Double strideFrequency;
    private Double verticalPower;
    private Double verticalDisplacement;
    private Double symmetry;
    private Double regularity;
    private Double strideLength;
    private Double lateralPower;
    private Double horizontalPower;

    public AllureType getType() {
        return type;
    }

    public void setType(AllureType type) {
        this.type = type;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getStrideFrequency() {
        return strideFrequency;
    }

    public void setStrideFrequency(Double strideFrequency) {
        this.strideFrequency = strideFrequency;
    }

    public Double getVerticalPower() {
        return verticalPower;
    }

    public void setVerticalPower(Double verticalPower) {
        this.verticalPower = verticalPower;
    }

    public Double getVerticalDisplacement() {
        return verticalDisplacement;
    }

    public void setVerticalDisplacement(Double verticalDisplacement) {
        this.verticalDisplacement = verticalDisplacement;
    }

    public Double getSymmetry() {
        return symmetry;
    }

    public void setSymmetry(Double symmetry) {
        this.symmetry = symmetry;
    }

    public Double getRegularity() {
        return regularity;
    }

    public void setRegularity(Double regularity) {
        this.regularity = regularity;
    }

    public Double getStrideLength() {
        return strideLength;
    }

    public void setStrideLength(Double strideLength) {
        this.strideLength = strideLength;
    }

    public Double getLateralPower() {
        return lateralPower;
    }

    public void setLateralPower(Double lateralPower) {
        this.lateralPower = lateralPower;
    }

    public Double getHorizontalPower() {
        return horizontalPower;
    }

    public void setHorizontalPower(Double horizontalPower) {
        this.horizontalPower = horizontalPower;
    }
}
