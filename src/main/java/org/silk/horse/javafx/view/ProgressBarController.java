package org.silk.horse.javafx.view;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;

import org.silk.horse.GenerationTask;

import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ProgressBarController extends AbstractController {
    @FXML
    private Label timerLabel;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private ProgressIndicator progressIndicator;

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
    private NumberFormat twoDigitFormat = new DecimalFormat("00");
    private int elapsedTime = 0;

    private String formatSeconds(int seconds) {
        long minutes = seconds / 60;
        long secondsRemainder = seconds - (minutes * 60);
        return twoDigitFormat.format(minutes) + ":" + twoDigitFormat.format(secondsRemainder);
    }

    private Runnable buildRefreshTask() {
        return () -> {
            elapsedTime++;
            Platform.runLater(() -> {
                timerLabel.setText(formatSeconds(elapsedTime));
            });
        };
    }

    public void launch(Path exePath, Path workingDirPath, Path excelPath, Path htmlDir) throws Exception {
        Task fxTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                new GenerationTask(exePath, workingDirPath, excelPath, htmlDir, this::updateProgress).generate();
                scheduledExecutorService.shutdownNow();
                return null;
            }
        };
        progressBar.progressProperty().unbind();
        progressBar.progressProperty().bind(fxTask.progressProperty());
        progressIndicator.progressProperty().unbind();
        progressIndicator.progressProperty().bind(fxTask.progressProperty());
        new Thread(fxTask).start();
    }

    @FXML
    private void initialize() {
        scheduledExecutorService.scheduleAtFixedRate(buildRefreshTask(), 0, 1, TimeUnit.SECONDS);
        progressBar.setProgress(-1);
        progressIndicator.setProgress(-1);
    }
}
