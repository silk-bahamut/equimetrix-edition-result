package org.silk.horse.javafx.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;

public class PrepareGenerationController extends AbstractController {
    @FXML
    private Label excelLabel;
    @FXML
    private Label workingDirLabel;
    @FXML
    private Button generate;


    @FXML
    private void chooseExcel() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Sélectioner un fichier excel");
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Fichiers Excel", "*.xls", "*.xlsx", "*.xlsm"));
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
        if (file != null) {
            excelLabel.setText(file.toPath().toString());
            mainApp.setExcelPath(file.toPath());
            checkReady();
        }
    }

    @FXML
    private void chooseWorkingDir() {
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle("Sélectionner un répertoire de travail");
        File file = fileChooser.showDialog(mainApp.getPrimaryStage());
        if (file != null) {
            workingDirLabel.setText(file.toPath().toString());
            mainApp.setWorkingDirPath(file.toPath());
            checkReady();
        }
    }

    @FXML
    private void launchProgress() throws Exception {
        mainApp.showProgressView();
    }

    private void checkReady() {
        if (mainApp.checkReadyToGenerate()) {
            generate.setDisable(false);
        }
    }

}
