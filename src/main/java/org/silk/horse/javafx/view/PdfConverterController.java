package org.silk.horse.javafx.view;

import javafx.fxml.FXML;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PdfConverterController extends AbstractController {
    @FXML
    private void openUrl() throws IOException {
        Desktop.getDesktop().browse(URI.create("http://wkhtmltopdf.org/downloads.html"));
    }

    @FXML
    public void checkInstallation() {
        if (checkPath("C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe") || checkPath("C:\\Program Files (x86)\\wkhtmltopdf\\bin\\wkhtmltopdf.exe")) {
            mainApp.showPrepareGenerationView();
        }
    }

    private boolean checkPath(String path) {
        boolean out = false;
        Path p = Paths.get(path);
        if (Files.exists(p)) {
            mainApp.setExePath(p);
            out = true;
        }
        return out;
    }
}
