package org.silk.horse.javafx.view;

import org.silk.horse.javafx.JavaFXMainApp;

public abstract class AbstractController {
    protected JavaFXMainApp mainApp;

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp the main application
     */
    public void setMainApp(JavaFXMainApp mainApp) {
        this.mainApp = mainApp;
    }
}
