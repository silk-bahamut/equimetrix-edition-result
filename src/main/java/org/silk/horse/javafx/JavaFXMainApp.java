package org.silk.horse.javafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import org.silk.horse.javafx.view.PdfConverterController;
import org.silk.horse.javafx.view.PrepareGenerationController;
import org.silk.horse.javafx.view.ProgressBarController;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JavaFXMainApp extends Application {
    // Java FX Fields
    private Stage primaryStage;
    private BorderPane rootLayout;
    // execution fields
    private Path exePath;
    private Path excelPath;
    private Path workingDirPath;
    private Path htmlPath;

    @Override
    public void start(Stage primaryStage) {
        htmlPath = Paths.get(getParameters().getRaw().get(0));
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Equimetrix edition result");
        initRootLayout();
        showPdfConverterView();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXMainApp.class.getResource("/view/RootLayout.fxml"));
        rootLayout = loadWithoutException(loader);

        // Show the scene containing the root layout.
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showPdfConverterView() {
        // Load pdf converter overview.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXMainApp.class.getResource("/view/PdfConverter.fxml"));
        AnchorPane pdfConverterView = loadWithoutException(loader);

        // Set PdfConverter overview into the center of root layout.
        rootLayout.setCenter(pdfConverterView);

        // Give the controller access to the main app.
        PdfConverterController controller = loader.getController();
        controller.setMainApp(this);
        controller.checkInstallation();
    }

    public void showPrepareGenerationView() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXMainApp.class.getResource("/view/PrepareGeneration.fxml"));
        AnchorPane prepareGenerationView = loadWithoutException(loader);

        rootLayout.setCenter(prepareGenerationView);

        PrepareGenerationController controller = loader.getController();
        controller.setMainApp(this);
    }

    public void showProgressView() throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXMainApp.class.getResource("/view/ProgressBar.fxml"));
        AnchorPane progressBarView = loadWithoutException(loader);

        rootLayout.setCenter(progressBarView);

        ProgressBarController controller = loader.getController();
        controller.setMainApp(this);
        controller.launch(exePath, workingDirPath, excelPath, htmlPath);
    }

    private <T> T loadWithoutException(FXMLLoader loader) {
        try {
            return loader.load();
        } catch (IOException e) {
            // should not append
            throw new RuntimeException("check your code, fxml not found", e);
        }
    }


    public boolean checkReadyToGenerate() {
        return exePath != null && excelPath != null && workingDirPath != null;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setExePath(Path exePath) {
        this.exePath = exePath;
    }

    public void setWorkingDirPath(Path workingDirPath) {
        this.workingDirPath = workingDirPath;
    }

    public void setExcelPath(Path excelPath) {
        this.excelPath = excelPath;
    }

    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
}
