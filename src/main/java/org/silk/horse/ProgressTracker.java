package org.silk.horse;

@FunctionalInterface
public interface ProgressTracker {
    void setProgress(int generated, int maximum);
}
