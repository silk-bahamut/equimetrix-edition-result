package org.silk.horse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.silk.horse.entity.Allure;
import org.silk.horse.entity.AllureType;
import org.silk.horse.entity.Horse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class GenerationTask {
    private ObjectMapper mapper;
    private Path htmlDir;
    private Path exePath;
    private Path workingDirPath;
    private Path excelPath;
    private int maximum;
    private int generatedFiles;
    private ProgressTracker callback;

    public GenerationTask(Path exePath, Path workingDirPath, Path excelPath, Path htmlDir, ProgressTracker callback) {
        this.exePath = exePath;
        this.workingDirPath = workingDirPath;
        this.excelPath = excelPath;
        this.htmlDir = htmlDir;
        this.callback = callback;

        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        maximum = 0;
        generatedFiles = 0;
    }

    public void generate() {
        try {
            LocalTime start = LocalTime.now();
            Path indexPath = copyFiles();
            List<Horse> horses = loadHorses();
            maximum = horses.size();
            for (Horse horse : horses) {
                executeCreation(indexPath, horse.getName(), horse);
            }
            cleanUp();
            Duration duration = Duration.between(start, LocalTime.now());
            System.out.println(DateTimeFormatter.ofPattern("m:ss").format(LocalTime.MIDNIGHT.plus(duration)));
        } catch (Exception e) {
            callback.setProgress(1, 1);
            e.printStackTrace();
        }
    }

    private Path copyFiles() throws IOException {
        Copy.TreeCopier tc = new Copy.TreeCopier(htmlDir, workingDirPath, false, false);
        Files.walkFileTree(htmlDir, tc);
        return workingDirPath.resolve("index.html");
    }

    private void executeCreation(Path indexHtml, String outPdfName, Horse horse) throws IOException, InterruptedException {
        // pdf
        Path out = workingDirPath.resolve(outPdfName + ".pdf");
        Files.deleteIfExists(out);

        // js
        Path jsPath = workingDirPath.resolve("js/horse.js");
        Files.deleteIfExists(jsPath);
        String s = String.format("var horse=%s;", mapper.writeValueAsString(horse));
        Files.write(jsPath, Collections.singleton(s));

        // command line
        String commandLine = String.format("\"%s\" \"%s\" \"%s\"", exePath, indexHtml, out);
        System.out.println("Generation de " + outPdfName);
        Process p = Runtime.getRuntime().exec(commandLine);
        if (!p.waitFor(1, TimeUnit.MINUTES)) {
            System.out.println(outPdfName + " failed");
        }
        generatedFiles++;
        callback.setProgress(generatedFiles, maximum);
    }

    private void cleanUp() throws IOException {
        RecursiveDelete.delete(workingDirPath, null, file -> "pdf".equals(FilenameUtils.getExtension(file.toString())), false);
    }

    private List<Horse> loadHorses() throws IOException {
        System.out.println("start loading horses");
        Workbook wb;
        if ("xls".equals(FilenameUtils.getExtension(excelPath.getFileName().toString()))) {
            wb = new HSSFWorkbook(Files.newInputStream(excelPath));
        } else {
            wb = new XSSFWorkbook(Files.newInputStream(excelPath));
        }

        Map<String, Horse> allHorses = new HashMap<>();
        Sheet s = wb.getSheetAt(0);
        boolean first = true;
        for (Row row : s) {
            if (first) {
                first = false;
                continue;
            }
            String sire = row.getCell(convertColumn('A')).getStringCellValue();
            Horse h = allHorses.get(sire);
            if (h == null) {
                h = new Horse(sire);
                allHorses.put(sire, h);
            }

            h.setName(row.getCell(convertColumn('B')).getStringCellValue());
            h.setSex(row.getCell(convertColumn('W')).getStringCellValue());
            h.setAge(new Double(row.getCell(convertColumn('X')).getNumericCellValue()).intValue());
            h.setRace(row.getCell(convertColumn('Y')).getStringCellValue());
            h.setFather(row.getCell(convertColumn('Z')).getStringCellValue());

            AllureType type = AllureType.valueOf(row.getCell(convertColumn('C')).getStringCellValue());
            h.addRaw(readAllure(row, type, 'D', false));
            h.addComputed(readAllure(row, type, 'M', true));
        }
        System.out.println("end loading horses");
        return allHorses.values()
                .stream()
                .sorted((h1, h2) -> h1.getSire().compareTo(h2.getSire()))
                .collect(Collectors.toList());
    }

    private Allure readAllure(Row row, AllureType type, char startingChar, boolean percent) {
        Allure allure = new Allure();
        allure.setType(type);
        allure.setSpeed(readValue(row, startingChar++, percent));
        allure.setStrideFrequency(readValue(row, startingChar++, percent));
        allure.setStrideLength(readValue(row, startingChar++, percent));
        allure.setRegularity(readValue(row, startingChar++, percent));
        allure.setSymmetry(readValue(row, startingChar++, percent));
        allure.setVerticalDisplacement(readValue(row, startingChar++, percent));
        allure.setVerticalPower(readValue(row, startingChar++, percent));
        allure.setHorizontalPower(readValue(row, startingChar++, percent));
        allure.setLateralPower(readValue(row, startingChar, percent));
        return allure;
    }

    private int convertColumn(char c) {
        return c - 'A';
    }

    private Double readValue(Row row, char c, boolean percent) {
        Cell cell = row.getCell(convertColumn(c));
        Double d = null;
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            d = cell.getNumericCellValue();
            if (percent) {
                d *= 100;
            }
        }
        return d;
    }
}
