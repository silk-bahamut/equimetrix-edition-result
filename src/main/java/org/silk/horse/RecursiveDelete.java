package org.silk.horse;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class RecursiveDelete {
    public static void delete(Path path, DirectoryStream.Filter<Path> dirFilter, DirectoryStream.Filter<Path> fileFilter, boolean deleteRoot) throws IOException {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return dirFilter == null || !dirFilter.accept(dir) ? FileVisitResult.CONTINUE : FileVisitResult.SKIP_SUBTREE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                if (attributes.isRegularFile() && (fileFilter == null || !fileFilter.accept(file))) {
                    Files.delete(file);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path directory, IOException ioe) throws IOException {
                if (deleteRoot || !directory.equals(path)) {
                    try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
                        if (!dirStream.iterator().hasNext()) {
                            Files.delete(directory);
                        }
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });

    }
}
